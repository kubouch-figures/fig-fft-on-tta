% Instruction schedule of a full 16-point FFT
%
% Required libraries:
% \usetikzlibrary{
%   calc,
%   backgrounds,
%   positioning,
%   patterns,
%   decorations.pathreplacing,
% }

% Fill grid from coordinate (#1, #2) certain number of squares to the right
% (#3) and down (#4) using a style #5.
\newcommand{\fillgrid} [5] {
  \foreach \x [evaluate=\x as \nx using \x-1] in {1, ..., #3} {
    \foreach \y [evaluate=\y as \ny using \y-1] in {1, ..., #4} {
      \fill [#5] (#1+\nx, -#2-\ny) rectangle ++(1, -1);
    }
  }
}

% Draw the whole butterfly with #1 offset and #2 fill style.
\newcommand{\drawbutterfly} [2] {
  \fillgrid{#1}{0}{4}{3}{#2};
  \fillgrid{#1+2}{3}{4}{2}{#2};
  \fillgrid{#1+6}{5}{4}{2}{#2};
  \fillgrid{#1+9}{7}{4}{1}{#2};
  \fillgrid{#1+13}{8}{4}{2}{#2};
  \fillgrid{#1+6}{10}{4}{1}{#2};
}

\tikzstyle{bfly1}   = [color=gray!50]
\tikzstyle{bfly2}   = [color=gray]
\tikzstyle{divline} = [very thick]

\begin{tikzpicture}
  % Full table
  \begin{scope}
    % Draw grid
    \draw [step=1, gray!20] (0, 0) grid (45, -11);

    % Fill boxes
    \begin{scope}[on background layer]
      \drawbutterfly{0}{bfly1};
      \drawbutterfly{4}{bfly2};
      \drawbutterfly{8}{bfly1};
      \drawbutterfly{12}{bfly2};
      \drawbutterfly{16}{bfly1};
      \drawbutterfly{20}{bfly2};
      \drawbutterfly{24}{bfly1};
      \drawbutterfly{28}{bfly2};
    \end{scope}

    % Clk labels
    \foreach \clk [evaluate=\clk as \x using \clk+0.5] in {0, ..., 44} {
      \node [minimum size=1, anchor=mid] (clk\clk) at (\x, 0.5) {\clk};
    }

    % Bus coordinates
    \foreach \bus [evaluate=\bus as \y using -\bus-0.5] in {0, ..., 9} {
      \node [left] (B\bus) at (0, \y) {B\bus};
    }
    \node [left] (b) at (0, -10.5) {b};

    % Prologue/kernel/epilogue dividers and labels
    \draw [divline] (13, 1) -- ++(0, -13) coordinate (sep1);
    \draw [divline] (32, 1) -- ++(0, -13) coordinate (sep2);

    \coordinate (labelstart) at (0, -11.5);
    \coordinate (labelend) at (45, -11.5);
    \node at ($(labelstart)!0.5!(sep1 |- labelstart)$)
      {Prologue};
    \node at ($(sep1 |- labelstart)!0.5!(sep2 |- labelstart)$)
      {Kernel};
    \node at ($(sep2 |- labelstart)!0.5!(labelend |- labelstart)$)
      {Epilogue};
  \end{scope}

\end{tikzpicture}
