% Graph showing power consumption
%
% Requires pgfplots

% This downloads the data. Compile with `pdflatex --shell-escape`.
% \immediate\write18{./get_results.sh}

\begin{tikzpicture}
  \tikzstyle{dataline} = [mark options=solid, mark=+]
  \begin{axis}
    [
      /pgf/number format/.cd, 1000 sep={},
      xlabel           = {Frequency (MHz)},
      xlabel style     = {yshift=-10pt},
      ylabel           = {FFT/mJ},
      ylabel near ticks,
      xmin             = 0,
      xmax             = 1200,
      xtick            = {0, 100, 200, ..., 1200},
      minor xtick      = {50, 100, ..., 1200},
      xticklabel style = {rotate=75, anchor=east},
      ymin             = 0,
      ymax             = 110000,
      minor y tick num = 1,  % ytick = {x, y, ..., z} works only for z<16384
      try min ticks    = 6,
      scaled y ticks   = base 10:-3,
      grid             = both,
      major grid style = {dotted,gray!95},
      minor grid style = {dotted,gray!50},
      legend columns   = 3,
      legend style     = {at={(0.5,1.05)}, anchor=south},
    ]
    \addplot [dataline, blue]
      table[x=f (MHz), y=FFT/mJ, col sep=comma] {csv/res_256_28nm_0.60v.csv};
    \addplot [dataline, cyan]
      table[x=f (MHz), y=FFT/mJ, col sep=comma] {csv/res_256_28nm_0.80v.csv};
    \addplot [dataline, green]
      table[x=f (MHz), y=FFT/mJ, col sep=comma] {csv/res_256_28nm_0.85v.csv};
    \addplot [dataline, yellow!30!lime]
      table[x=f (MHz), y=FFT/mJ, col sep=comma] {csv/res_256_28nm_0.90v.csv};
    \addplot [dataline, orange]
      table[x=f (MHz), y=FFT/mJ, col sep=comma] {csv/res_256_28nm_0.95v.csv};
    \addplot [dataline, red]
      table[x=f (MHz), y=FFT/mJ, col sep=comma] {csv/res_256_28nm_1.10v.csv};
    \addplot [dataline, gray]
      table[x=f (MHz), y=FFT/mJ, col sep=comma] {csv/res_256_65nm_1.00v.csv};
  %   \legend{28nm/0.60V, 28nm/0.80V, 28nm/0.85V, 28nm/0.90V, 28nm/0.95V, 28nm/1.10V, 65nm/1.00V}
  \end{axis}
\end{tikzpicture}
