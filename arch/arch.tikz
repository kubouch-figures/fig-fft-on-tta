% TTA Architecture figure with an interconnection network, FUs and memories.
%
% Required libraries (load with \usetikzlibrary{..}):
%   decorations.markings, calc, positioning, arrows.meta

\def\sq{0.4cm}
\def\fuheight{4*\sq}
\def\yfuhi{2*\sq+\fuheight/2}
\def\yfulo{-12*\sq-\fuheight/2}
\def\yfulolo{\yfulo-2*\sq-\fuheight}
\def\circ{0.6mm}


\begin{tikzpicture}
  % Styles
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Arrow -->--
  \tikzset{
    ->-/.style={
      decoration={
        markings,
        mark=at position #1 with {\arrow{Stealth[scale=.8]}}
      },
      postaction={decorate}
    }
  }
  % Arrow --<--
  \tikzset{
    -<-/.style={
      decoration={
        markings,
        mark=at position #1 with {\arrow{Stealth[reversed, scale=.8]}}
      },
      postaction={decorate}
    }
  }
  % Other styles
  \tikzstyle{bush}    = [very thick]
  \tikzstyle{busl}    = [thin]
  \tikzstyle{box}     = [bush, draw, rounded corners]
  \tikzstyle{fubox}   = [box, minimum size=\fuheight]
  \tikzstyle{lutbox}  = [box, minimum width=\fuheight, minimum height=2*\sq,
                         rotate=90]
  \tikzstyle{dmembox} = [box, minimum width=3*\fuheight+\sq,
                         minimum height=\fuheight]
  \tikzstyle{imembox} = [box, minimum width=2*\fuheight,
                         minimum height=\fuheight]
  \tikzstyle{si}      = [bush, -<-=1.1*\sq]
  \tikzstyle{sithin}  = [bush, -<-=1.05*\sq, thin]
  \tikzstyle{so}      = [bush, ->-=1.2*\sq]
  \tikzstyle{sothin}  = [bush, ->-=1.05*\sq, thin]
  \tikzstyle{sogcu}   = [bush, ->-=1.4*\sq]
  \tikzstyle{biarrow} = [bush, <->, >=stealth]
  \tikzstyle{arrow}   = [bush, ->, >=stealth]
  \tikzstyle{circ}    = [fill, circle, inner sep=\circ]

  % Buses
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \foreach \i in {0,1,2,3,4,5,6,7,8,9} {
    \draw[bush] (0,-\i*\sq) node[left] (B\i) {B\i} -- (45*\sq, -\i*\sq);
  }
  \draw[busl] (0,-10*\sq) node[left] (b) {b} -- (45*\sq,-10*\sq);

  % Socket coordinates
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \coordinate (addi0)   at (   \sq, 0);
  \coordinate (addi1)   at ( 2*\sq, 0);
  \coordinate (addo0)   at ( 3*\sq, 0);

  \coordinate (tfgi0)   at ( 5*\sq, 0);
  \coordinate (tfgi1)   at ( 6*\sq, 0);
  \coordinate (tfgo0)   at ( 7*\sq, 0);
  \coordinate (tfgo1)   at ( 8*\sq, 0);

  \coordinate (lsuri0)  at (10*\sq, 0);
  \coordinate (lsuri1)  at (11*\sq, 0);
  \coordinate (lsuro0)  at (12*\sq, 0);

  \coordinate (agi0)    at (14*\sq, 0);
  \coordinate (agi1)    at (15*\sq, 0);
  \coordinate (agi2)    at (16*\sq, 0);
  \coordinate (ago0)    at (17*\sq, 0);

  \coordinate (lsuwi0)  at (19*\sq, 0);
  \coordinate (lsuwi1)  at (20*\sq, 0);
  \coordinate (lsuwo0)  at (21*\sq, 0);

  \coordinate (dlyi0)   at (23*\sq, 0);
  \coordinate (dlyo0)   at (24*\sq, 0);

  \coordinate (shi0)    at (26*\sq, 0);
  \coordinate (shi1)    at (27*\sq, 0);
  \coordinate (sho0)    at (28*\sq, 0);

  \coordinate (cmuli0)  at (30*\sq, 0);
  \coordinate (cmuli1)  at (31*\sq, 0);
  \coordinate (cmulo0)  at (32*\sq, 0);

  \coordinate (rfi0)    at (34*\sq, 0);
  \coordinate (rfo0)    at (35*\sq, 0);

  \coordinate (caddi0)  at (37*\sq, 0);
  \coordinate (caddi1)  at (38*\sq, 0);
  \coordinate (caddo0)  at (39*\sq, 0);

  \coordinate (gcui0)   at (41*\sq, 0);
  \coordinate (gcui1)   at (42*\sq, 0);
  \coordinate (gcuo0)   at (43*\sq, 0);
  \coordinate (gcuo1)   at (44*\sq, 0);

  % FUs above
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \path let \p1 = (tfgi0),
            \p2 = (tfgo1)  in (\x1/2+\x2/2, \yfuhi)
                                            node[fubox] (tfg)  {TFG};
  \path let \p1 = (agi1),
            \p2 = (agi2)   in (\x1/2+\x2/2, \yfuhi)
                                            node[fubox] (ag)   {AG};
  \path let \p1 = (dlyi0),
            \p2 = (dlyo0)  in (\x1/2+\x2/2, \yfuhi)
                                            node[fubox] (dly)  {DLY};
  \path let \p1 = (cmuli1) in (\x1, \yfuhi) node[fubox] (cmul) {CMUL};
  \path let \p1 = (caddi1) in (\x1, \yfuhi) node[fubox] (cadd) {CADD};

  % FUs below
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \path let \p1 = (addi1)  in (\x1, \yfulo) node[fubox] (add)  {ADD};
  \path let \p1 = (lsuri1) in (\x1, \yfulo) node[fubox] (lsur) {LSUr};
  \path let \p1 = (lsuwi1) in (\x1, \yfulo) node[fubox] (lsuw) {LSUw};
  \path let \p1 = (shi1)   in (\x1, \yfulo) node[fubox] (sh)   {SH};
  \path let \p1 = (rfi0),
            \p2 = (rfo0)   in (\x1/2+\x2/2, \yfulo)
                                            node[fubox] (rf)   {RF};
  \path let \p1 = (gcui1),
            \p2 = (gcuo0)  in (\x1/2+\x2/2, \yfulo)
                                            node[fubox] (gcu)  {GCU};

  % Memories below below
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \path let \p1 = (ag) in (\x1, \yfulolo) node[dmembox] (dmem) {Data Memory};
  \path let \p1 =
    (gcu.west) in (\x1, \yfulolo) node[imembox] (imem) {Instruction Memory};

  % Sockets
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \draw[si]       (addi0  |- add.north)                  -- (addi0  |- B0);
  \draw[si]       (addi1  |- add.north)  node[below] {*} -- (addi1  |- B0);
  \draw[so]       (addo0  |- add.north)                  -- (addo0  |- B0);

  \draw[si]       (tfgi0  |- tfg.south)                  -- (tfgi0  |- b);
  \draw[si]       (tfgi1  |- tfg.south)  node[above] {*} -- (tfgi1  |- b);
  \draw[so]       (tfgo0  |- tfg.south)                  -- (tfgo0  |- b);
  \draw[sothin]   (tfgo1  |- tfg.south)                  -- (tfgo1  |- b);

  \draw[si, gray] (lsuri0 |- lsur.north)                 -- (lsuri0 |- B0);
  \draw[si]       (lsuri1 |- lsur.north) node[below] {*} -- (lsuri1 |- B0);
  \draw[so]       (lsuro0 |- lsur.north)                 -- (lsuro0 |- B0);

  \draw[si]       (agi0   |- ag.south)                   -- (agi0   |- b);
  \draw[si]       (agi1   |- ag.south)                   -- (agi1   |- b);
  \draw[si]       (agi2   |- ag.south)   node[above] {*} -- (agi2   |- b);
  \draw[so]       (ago0   |- ag.south)                   -- (ago0   |- b);

  \draw[si]       (lsuwi0 |- lsuw.north)                 -- (lsuwi0 |- B0);
  \draw[si]       (lsuwi1 |- lsuw.north) node[below] {*} -- (lsuwi1 |- B0);
  \draw[so, gray] (lsuwo0 |- lsuw.north)                 -- (lsuwo0 |- B0);

  \draw[si]       (dlyi0  |- dly.south)  node[above] {*} -- (dlyi0  |- b);
  \draw[so]       (dlyo0  |- dly.south)                  -- (dlyo0  |- b);

  \draw[si]       (shi0   |- sh.north)                   -- (shi0   |- B0);
  \draw[si]       (shi1   |- sh.north)   node[below] {*} -- (shi1   |- B0);
  \draw[so]       (sho0   |- sh.north)                   -- (sho0   |- B0);

  \draw[si]       (cmuli0 |- cmul.south)                 -- (cmuli0 |- b);
  \draw[si]       (cmuli1 |- cmul.south) node[above] {*} -- (cmuli1 |- b);
  \draw[so]       (cmulo0 |- cmul.south)                 -- (cmulo0 |- b);

  \draw[si]       (rfi0   |- rf.north)                   -- (rfi0   |- B0);
  \draw[so]       (rfo0   |- rf.north)                   -- (rfo0   |- B0);

  \draw[sithin]   (caddi0 |- cadd.south)                 -- (caddi0 |- b);
  \draw[si]       (caddi1 |- cadd.south) node[above] {*} -- (caddi1 |- b);
  \draw[so]       (caddo0 |- cadd.south)                 -- (caddo0 |- b);

  \draw[si]       (gcui0  |- gcu.north)                  -- (gcui0  |- B0);
  \draw[si]       (gcui1  |- gcu.north)  node[below] {*} -- (gcui1  |- B0);
  \draw[so]       (gcuo0  |- gcu.north)                  -- (gcuo0  |- B0);
  \draw[sogcu]    (gcuo0  |- gcu.north)  -- ++(\sq, \sq) -- (gcuo1 |- B0);

  % Dots
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \node[circ]       at (addi0  |- B5) {};
  \node[circ]       at (addi1  |- B0) {};
  \node[circ]       at (addo0  |- B0) {};
  \node[circ]       at (addo0  |- B1) {};
  \node[circ]       at (addo0  |- B2) {};

  \node[circ]       at (tfgi0  |- B4) {};
  \node[circ]       at (tfgi1  |- B2) {};
  \node[circ]       at (tfgo0  |- B6) {};
  \node[circ]       at (tfgo1  |- b)  {};

  \node[circ, gray] at (lsuri0 |- B9) {};
  \node[circ]       at (lsuri1 |- B3) {};
  \node[circ]       at (lsuro0 |- B5) {};

  \node[circ]       at (agi0   |- B5) {};
  \node[circ]       at (agi1   |- B4) {};
  \node[circ]       at (agi2   |- B1) {};
  \node[circ]       at (ago0   |- B3) {};
  \node[circ]       at (ago0   |- B4) {};

  \node[circ]       at (lsuwi0 |- B8) {};
  \node[circ]       at (lsuwi1 |- B9) {};
  \node[circ, gray] at (lsuwo0 |- B9) {};

  \node[circ]       at (dlyi0  |- B4) {};
  \node[circ]       at (dlyo0  |- B9) {};

  \node[circ]       at (shi0   |- B1) {};
  \node[circ]       at (shi1   |- B4) {};
  \node[circ]       at (sho0   |- B1) {};
  \node[circ]       at (sho0   |- B3) {};
  \node[circ]       at (sho0   |- B5) {};

  \node[circ]       at (cmuli0 |- B5) {};
  \node[circ]       at (cmuli1 |- B6) {};
  \node[circ]       at (cmulo0 |- B7) {};

  \node[circ]       at (rfi0   |- B3) {};
  \node[circ]       at (rfo0   |- B4) {};
  \node[circ]       at (rfo0   |- B5) {};

  \node[circ]       at (caddi0 |- b)  {};
  \node[circ]       at (caddi1 |- B7) {};
  \node[circ]       at (caddo0 |- B8) {};

  \node[circ]       at (gcui0  |- B2) {};
  \node[circ]       at (gcui1  |- B8) {};
  \node[circ]       at (gcui1  |- B9) {};
  \node[circ]       at (gcuo0  |- B8) {};
  \node[circ]       at (gcuo0  |- B9) {};
  \node[circ]       at (gcuo1  |- B8) {};
  \node[circ]       at (gcuo1  |- B9) {};

  % Other
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \node[lutbox] (lut) at (2*\sq, \yfuhi) {LUT};
  \draw[biarrow] (lut) -- (tfg);

  \draw[biarrow] (lsur.south) -- (lsur.south |- dmem.north);
  \draw[biarrow] (lsuw.south) -- (lsuw.south |- dmem.north);

  \draw[arrow] (gcu.south |- imem.north) -- (gcu.south);

\end{tikzpicture}
