% Address generation for all stages (128-point FFT)
%
% Required libraries:
% \usetikzlibrary{
%   backgrounds,
%   calc,
%   decorations.pathreplacing,
% }

% Fill a grid line with 'b_x' with x in range #1 starting at (#2, #3).
% b0 and b1 are filled with #4 style.
\newcommand{\drawbits} [4] {
  \foreach \b [count=\x from 0] in #1 {
    \node at (\x+#2+0.5, -#3-0.5) {$b_\b$};
    \begin{scope}[on background layer]
      \ifnum\b=0
        \fill [#4] (\x+#2, -#3) rectangle ++(1, -1);
      \fi
      \ifnum\b=1
        \fill [#4] (\x+#2, -#3) rectangle ++(1, -1);
      \fi
    \end{scope}
  }
}

% Fill a grid line with 'x' with x in range #1 starting at (#2, #3).
\newcommand{\drawvalues} [3] {
  \foreach \b [count=\x from 0] in #1 {
    \node at (\x+#2+0.5, -#3-0.5) {\b};
  }
}

\tikzstyle{lsbs} = [gray!30]
\tikzstyle{gridline} = [gray!50]

\begin{tikzpicture}
  % Draw grid and thick lines
  \draw [step=1, gridline] (0, 0) grid (9, -5);
  \draw [thick] (2, 0) -- (2, -5);
  \draw [thick] (0, -1) -- +(9, 0);

  % Draw bits and values
  \drawbits{{8,7,6,5,4,3,2,1,0}}{0}{0}{lsbs};
  \drawbits{{1,0,6,5,4,3,2}}{2}{1}{lsbs};
  \drawbits{{6,5,1,0,4,3,2}}{2}{2}{lsbs};
  \drawbits{{6,5,4,3,1,0,2}}{2}{3}{lsbs};
  \drawbits{{6,5,4,3,2,1,0}}{2}{4}{lsbs};
  \drawvalues{{0,0}}{0}{1};
  \drawvalues{{0,1}}{0}{2};
  \drawvalues{{1,0}}{0}{3};
  \drawvalues{{1,1}}{0}{4};

  % Text labels
  \draw [decorate, decoration={brace, amplitude=10pt}] (0, 0)
    -- node [above=8pt] {stage} (2, 0);
  \draw [decorate, decoration={brace, amplitude=10pt}] (2, 0)
    -- node [above=10pt] {index} (9, 0);

\end{tikzpicture}
